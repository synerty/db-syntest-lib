# Custom PRO file
# Allows project specific options such as libs and includes
# Includes notes from the manual
# For more info, See http://doc.trolltech.com/4.6/qmake-project-files.html
#
# =============================================================================
# Version History 
#
# <version>	<yymmdd_hhmm>	<Name>
#	<comment ....>
# =============================================================================
#
# 0003		100519_1344		Jarrod Chesney
#	Updated boost comments
# 0002		100504_1402		Jarrod Chesney
#	Converted stricture to the modular libs/???.pri format
# 0001		100226_2008		Jarrod Chesney
#	Initial Writing
#
# =============================================================================


# Declaring Other Libraries
# If you are using other libraries in your project in addition
# to those supplied with Qt, you need to specify them in your project file.

# The paths that qmake searches for libraries and the specific
# libraries to link against can be added to the list of values
# in the LIBS variable.
# The paths to the libraries themselves can be given, or the familiar
# Unix-style notation for specifying libraries and paths can be
# used if preferred.

# For example, the following lines show how a library can be specified:
# LIBS += -L/usr/local/lib -lmath

# The paths containing header files can also be specified in a similar
# way using the INCLUDEPATH variable.
# For example, it is possible to add several paths to be searched for
# header files:
#INCLUDEPATH = c:/msdev/include d:/stl/include

# NOTE : The libs for the development environment are added from their respective
# lib/xxx.pri files. You only need to add the additional libs you want and then
# include the pri files.
# This allows the individual lib project files to be improved with out the need for
# editing this file.
#
# Some Examples
#

# Include the WX libs
# NOTE : Adding WX removes all QT libs, headers and defines
# Add the advanced component for WX
# WX += adv
# include(../../build/libs/wx.pri)

# Include the BOOST libs and headers
# Add the libs to this variable
# BOOST += math_c99f date_time
# include(../../build/libs/boost.pri)

# Include the DTL lib
# include(../../build/libs/dtl.pri)


QT += sql

include(../../build/libs/boost.pri)

INCLUDEPATH += "../../../RsOrm/Source"
LIBS += -L'../../../RsOrm/Output/Release32Shared/'
LIBS += -lRsOrm

INCLUDEPATH += "../../Source"

TEMPLATE = lib
CONFIG += dll
DEFINES += BUILD_DB_SYNTEST_DLL BUILD_ORM_V1_DLL
LIBS += -shared

# QMAKE_CXXFLAGS_RELEASE += -g0 -O0