--
-- PostgreSQL database dump
--

-- Dumped from database version 9.0.4
-- Dumped by pg_dump version 9.0.4
-- Started on 2015-10-19 22:44:07

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 7 (class 2615 OID 55774)
-- Name: sams_meta; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sams_meta;


ALTER SCHEMA sams_meta OWNER TO postgres;

SET search_path = sams_meta, pg_catalog;

--
-- TOC entry 1517 (class 1259 OID 55775)
-- Dependencies: 7
-- Name: seq_ID; Type: SEQUENCE; Schema: sams_meta; Owner: postgres
--

CREATE SEQUENCE "seq_ID"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sams_meta."seq_ID" OWNER TO postgres;

--
-- TOC entry 1848 (class 0 OID 0)
-- Dependencies: 1517
-- Name: seq_ID; Type: SEQUENCE SET; Schema: sams_meta; Owner: postgres
--

SELECT pg_catalog.setval('"seq_ID"', 393, true);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1518 (class 1259 OID 55777)
-- Dependencies: 1801 1802 1803 1804 1805 7
-- Name: tbl_COLUMN; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_COLUMN" (
    "ID" integer NOT NULL,
    "NAME" character varying NOT NULL,
    "TABLE" integer NOT NULL,
    "VALUE_TYPE" integer,
    "DATA_TYPE" character varying(6) DEFAULT 'int4'::character varying NOT NULL,
    "DATA_LENGTH" integer,
    "IS_NULLABLE" boolean DEFAULT false NOT NULL,
    "RELATES_TO" integer,
    "DEFAULT_VALUE" character varying,
    "MANUALLY_MODIFIED" boolean DEFAULT false NOT NULL,
    "GENERATE_STORABLE_PROPERTY" boolean DEFAULT true NOT NULL,
    "PRIMARY_KEY" boolean DEFAULT false NOT NULL,
    "NICE_NAME" character varying
);


ALTER TABLE sams_meta."tbl_COLUMN" OWNER TO postgres;

--
-- TOC entry 1519 (class 1259 OID 55788)
-- Dependencies: 1806 7
-- Name: tbl_OBJECT_TYPE; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_OBJECT_TYPE" (
    "ID" integer DEFAULT nextval('"seq_ID"'::regclass) NOT NULL,
    "NAME" character varying,
    "INHERITS" integer,
    "DATA_TABLE" integer,
    "OBJECT_TYPE_TABLE" integer
);


ALTER TABLE sams_meta."tbl_OBJECT_TYPE" OWNER TO postgres;

--
-- TOC entry 1849 (class 0 OID 0)
-- Dependencies: 1519
-- Name: TABLE "tbl_OBJECT_TYPE"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON TABLE "tbl_OBJECT_TYPE" IS 'Describes the different types of objects.';


--
-- TOC entry 1850 (class 0 OID 0)
-- Dependencies: 1519
-- Name: COLUMN "tbl_OBJECT_TYPE"."INHERITS"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON COLUMN "tbl_OBJECT_TYPE"."INHERITS" IS 'The object type this object type inherits from.
IE :
Channel
-> DNP Channel';


--
-- TOC entry 1851 (class 0 OID 0)
-- Dependencies: 1519
-- Name: COLUMN "tbl_OBJECT_TYPE"."DATA_TABLE"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON COLUMN "tbl_OBJECT_TYPE"."DATA_TABLE" IS 'The table containing the data for this object variant.The table must be the last table in the joined table inheritance, IE the least common table.';


--
-- TOC entry 1520 (class 1259 OID 55795)
-- Dependencies: 7
-- Name: tbl_SCHEMA; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_SCHEMA" (
    "ID" integer NOT NULL,
    "NAME" character varying NOT NULL,
    "NICE_NAME" character varying
);


ALTER TABLE sams_meta."tbl_SCHEMA" OWNER TO postgres;

--
-- TOC entry 1852 (class 0 OID 0)
-- Dependencies: 1520
-- Name: TABLE "tbl_SCHEMA"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON TABLE "tbl_SCHEMA" IS 'Describes the schemas in this database';


--
-- TOC entry 1521 (class 1259 OID 55801)
-- Dependencies: 1807 1808 7
-- Name: tbl_TABLE; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_TABLE" (
    "ID" integer NOT NULL,
    "INHERITS" integer,
    "SCHEMA" integer NOT NULL,
    "NAME" character varying NOT NULL,
    "INHERIT_TYPE" character varying(8),
    "GENERATE_STORABLE" boolean DEFAULT true NOT NULL,
    "INHERITS_IS_USER_DEFINED" boolean DEFAULT false NOT NULL,
    "NICE_NAME" character varying
);


ALTER TABLE sams_meta."tbl_TABLE" OWNER TO postgres;

--
-- TOC entry 1522 (class 1259 OID 55809)
-- Dependencies: 1809 7
-- Name: tbl_VALUE; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_VALUE" (
    "ID" integer DEFAULT nextval('"seq_ID"'::regclass) NOT NULL,
    "VALUE_TYPE" integer NOT NULL,
    "NAME" character varying NOT NULL,
    "TOOLTIP" character varying,
    "HELP" character varying
);


ALTER TABLE sams_meta."tbl_VALUE" OWNER TO postgres;

--
-- TOC entry 1853 (class 0 OID 0)
-- Dependencies: 1522
-- Name: COLUMN "tbl_VALUE"."TOOLTIP"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON COLUMN "tbl_VALUE"."TOOLTIP" IS 'Enter the tooltop that will be displayed when this option is hovered over in the GUI.';


--
-- TOC entry 1854 (class 0 OID 0)
-- Dependencies: 1522
-- Name: COLUMN "tbl_VALUE"."HELP"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON COLUMN "tbl_VALUE"."HELP" IS 'Add application help for this value here.';


--
-- TOC entry 1523 (class 1259 OID 55816)
-- Dependencies: 1810 1811 7
-- Name: tbl_VALUE_TYPE; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_VALUE_TYPE" (
    "ID" integer DEFAULT nextval('"seq_ID"'::regclass) NOT NULL,
    "NAME" character varying NOT NULL,
    "COMMENT" character varying,
    "GENERATE_ENUM" boolean DEFAULT true NOT NULL,
    "TABLE" integer
);


ALTER TABLE sams_meta."tbl_VALUE_TYPE" OWNER TO postgres;

--
-- TOC entry 1855 (class 0 OID 0)
-- Dependencies: 1523
-- Name: COLUMN "tbl_VALUE_TYPE"."TABLE"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON COLUMN "tbl_VALUE_TYPE"."TABLE" IS 'The table containing the data for this value type.The table must be the last table in the joined table inheritance, IE the least common table.';


--
-- TOC entry 1840 (class 0 OID 55777)
-- Dependencies: 1518
-- Data for Name: tbl_COLUMN; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_COLUMN" ("ID", "NAME", "TABLE", "VALUE_TYPE", "DATA_TYPE", "DATA_LENGTH", "IS_NULLABLE", "RELATES_TO", "DEFAULT_VALUE", "MANUALLY_MODIFIED", "GENERATE_STORABLE_PROPERTY", "PRIMARY_KEY", "NICE_NAME") FROM stdin;
277	ALIAS	267	\N	string	0	t	\N	\N	f	t	f	\N
278	FUNCTION	267	\N	string	0	t	\N	\N	f	t	f	\N
279	ID	267	\N	string	0	f	\N	\N	f	t	t	\N
280	NAME	267	\N	string	0	t	\N	\N	f	t	f	\N
281	REQUIRES_USER	267	\N	int4	0	f	\N	\N	f	t	f	\N
283	STATE_TRANSLATION	267	\N	string	0	t	\N	\N	f	t	f	\N
285	VALUE_TRANSLATION	267	\N	string	0	t	\N	\N	f	t	f	\N
288	ID	268	\N	int4	0	t	\N	\N	f	t	t	\N
290	NAME	269	\N	string	0	f	\N	\N	f	t	t	\N
291	SIM_STATE	269	\N	string	0	f	\N	\N	f	t	f	\N
292	SYS_STATE	269	\N	string	0	f	\N	\N	f	t	f	\N
296	FEEDBACK_TIMEOUT	271	\N	int4	0	f	\N	\N	f	t	f	\N
298	TYPE	271	\N	string	0	f	\N	\N	f	t	f	\N
303	NAME	273	\N	string	0	f	\N	\N	f	t	t	\N
306	ID	274	\N	int4	0	f	\N	\N	f	t	t	\N
307	NAME	274	\N	string	0	f	\N	\N	f	t	f	\N
282	SIM_POINT	267	\N	string	0	f	297	\N	t	t	f	\N
284	SYSTEM	267	\N	string	0	f	303	\N	t	t	f	\N
346	TEST_POINT	270	\N	int4	0	f	288	\N	t	t	f	\N
286	BATCH	268	\N	int4	0	f	306	\N	t	t	f	\N
289	SIM_POINT	268	\N	string	0	f	297	\N	t	t	f	\N
352	COMMENT	350	\N	string	0	t	\N	\N	f	t	f	\N
353	DATE_FINISHED	350	\N	int4	0	t	\N	\N	f	t	f	\N
354	DATE_STARTED	350	\N	int4	0	t	\N	\N	f	t	f	\N
355	ID	350	\N	int4	0	f	\N	\N	f	t	t	\N
356	RUN_NUMBER	350	\N	int4	0	f	\N	\N	f	t	f	\N
371	SIM_DIG_STATE	270	\N	string	0	t	\N	\N	f	t	f	\N
328	COMMENT	274	\N	string	0	t	\N	\N	f	t	f	\N
329	DATE	326	\N	int4	0	t	\N	\N	f	t	f	\N
330	ID	326	\N	int4	0	f	\N	\N	f	t	t	\N
334	ID	324	\N	int4	0	f	\N	\N	f	t	t	\N
335	MESSAGE	324	\N	string	0	f	\N	\N	f	t	f	\N
337	SEVERITY	324	\N	string	0	f	\N	\N	f	t	f	\N
339	ID	325	\N	int4	0	f	\N	\N	f	t	t	\N
351	BATCH	350	\N	int4	0	f	306	\N	t	t	f	\N
332	TEST_VALUE	326	\N	int4	0	f	344	\N	t	t	f	\N
348	SIM_VALUE	327	\N	float8	0	f	\N	\N	f	t	f	\N
294	SIM_VALUE	270	\N	float8	0	t	\N	\N	f	t	f	\N
344	ID	270	\N	int4	0	f	\N	\N	f	t	t	\N
347	NAME	327	\N	int4	0	f	\N	\N	f	t	t	\N
331	TEST_RUN	326	\N	int4	0	f	355	\N	t	t	f	\N
338	BATCH	325	\N	int4	0	f	306	\N	t	t	f	\N
372	SIM_DIG_VALUE	270	\N	string	0	t	\N	\N	f	t	f	\N
340	SYSTEM	325	\N	string	0	f	303	\N	t	t	f	\N
349	SYS_VALUE	327	\N	float8	0	f	\N	\N	f	t	f	\N
358	AO_ENG_MAX	271	\N	float8	0	t	\N	\N	f	t	f	\N
359	AO_ENG_MIN	271	\N	float8	0	t	\N	\N	f	t	f	\N
323	ID	322	\N	int4	0	f	\N	\N	f	t	f	\N
357	PK	322	\N	int4	0	f	\N	\N	f	t	t	\N
302	AGENT_PORT	273	\N	int4	0	t	\N	\N	f	t	f	\N
336	RUN	324	\N	int4	0	t	355	\N	t	t	f	\N
297	SIM_ALIAS	271	\N	string	0	f	\N	\N	t	t	t	\N
373	PID	267	\N	string	0	f	\N	\N	f	t	f	\N
374	SUP1	267	\N	string	0	t	\N	\N	f	t	f	\N
375	SUP2	267	\N	string	0	t	\N	\N	f	t	f	\N
301	AGENT_IP	273	\N	string	0	t	\N	\N	f	t	f	\N
333	BATCH	324	\N	int4	0	t	306	\N	t	t	f	\N
363	ID	360	\N	int4	0	f	\N	\N	f	t	t	\N
368	SIMULATOR_ALIAS_POSTFIX	273	\N	string	0	t	\N	\N	f	t	f	\N
369	SIMULATOR_IP	273	\N	string	0	f	\N	\N	f	t	f	\N
364	SYS_POINT	360	\N	string	0	f	279	\N	t	t	f	\N
370	SIMULATOR_PORT	273	\N	int4	0	f	\N	\N	f	t	f	\N
365	TEST_VALUE	360	\N	int4	0	f	344	\N	t	t	f	\N
379	FEEDBACK_RECIEVED	378	\N	int4	0	t	\N	\N	f	t	f	\N
380	FEEDBACK_TIME	378	\N	int4	0	t	\N	\N	f	t	f	\N
382	SYS_DIG_STATE	378	\N	string	0	t	\N	\N	f	t	f	\N
385	SYS_POINT_RECIEVED	378	\N	string	0	t	\N	\N	f	t	f	\N
386	SYS_RAW_DIG_STATE	378	\N	string	0	t	\N	\N	f	t	f	\N
387	SYS_RAW_VALUE	378	\N	float8	0	t	\N	\N	f	t	f	\N
388	SYS_VALUE	378	\N	float8	0	t	\N	\N	f	t	f	\N
381	ID	378	\N	int4	0	t	\N	\N	t	t	t	\N
390	TEST_VALUE_RUN	378	\N	int4	0	t	330	\N	t	t	f	\N
384	SYS_EVENT_OK	378	\N	int4	0	t	\N	\N	f	t	f	\N
389	TEST_SYS_VALUE	378	\N	int4	0	t	363	\N	t	t	f	\N
383	SYS_DISPLAY_OK	378	\N	int4	0	t	\N	\N	f	t	f	\N
391	SYS_COMMENT	378	\N	string	0	t	\N	\N	f	t	f	\N
392	SYS_STATE_TEXT_OK	378	\N	int4	0	t	\N	\N	f	t	f	\N
393	SYS_VALUE_OK	378	\N	int4	0	t	\N	\N	f	t	f	\N
\.


--
-- TOC entry 1841 (class 0 OID 55788)
-- Dependencies: 1519
-- Data for Name: tbl_OBJECT_TYPE; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_OBJECT_TYPE" ("ID", "NAME", "INHERITS", "DATA_TABLE", "OBJECT_TYPE_TABLE") FROM stdin;
\.


--
-- TOC entry 1842 (class 0 OID 55795)
-- Dependencies: 1520
-- Data for Name: tbl_SCHEMA; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_SCHEMA" ("ID", "NAME", "NICE_NAME") FROM stdin;
266	st	\N
\.


--
-- TOC entry 1843 (class 0 OID 55801)
-- Dependencies: 1521
-- Data for Name: tbl_TABLE; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_TABLE" ("ID", "INHERITS", "SCHEMA", "NAME", "INHERIT_TYPE", "GENERATE_STORABLE", "INHERITS_IS_USER_DEFINED", "NICE_NAME") FROM stdin;
268	\N	266	TEST_POINT	\N	t	f	\N
269	\N	266	TRANS_STATE	\N	t	f	\N
267	\N	266	SYS_POINT	\N	t	t	\N
271	\N	266	SIM_POINT	\N	t	f	\N
273	\N	266	SYSTEM	\N	t	f	\N
274	\N	266	TEST_BATCH	\N	t	f	\N
322	\N	266	NEXT_ID	\N	t	f	\N
324	\N	266	TEST_LOG	\N	t	f	\N
325	\N	266	TEST_SYSTEM	\N	t	f	\N
270	\N	266	TEST_VALUE	\N	t	f	\N
326	\N	266	TEST_VALUE_RUN	\N	t	f	\N
327	\N	266	TRANS_VALUE	\N	t	f	\N
350	\N	266	TEST_RUN	\N	t	f	\N
360	\N	266	TEST_SYS_VALUE	\N	t	f	\N
378	\N	266	TEST_SYS_VALUE_RUN	\N	t	f	\N
\.


--
-- TOC entry 1844 (class 0 OID 55809)
-- Dependencies: 1522
-- Data for Name: tbl_VALUE; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_VALUE" ("ID", "VALUE_TYPE", "NAME", "TOOLTIP", "HELP") FROM stdin;
\.


--
-- TOC entry 1845 (class 0 OID 55816)
-- Dependencies: 1523
-- Data for Name: tbl_VALUE_TYPE; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_VALUE_TYPE" ("ID", "NAME", "COMMENT", "GENERATE_ENUM", "TABLE") FROM stdin;
\.


--
-- TOC entry 1813 (class 2606 OID 55825)
-- Dependencies: 1518 1518
-- Name: tbl_COLUMN_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_COLUMN"
    ADD CONSTRAINT "tbl_COLUMN_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1815 (class 2606 OID 55827)
-- Dependencies: 1519 1519 1519
-- Name: tbl_OBJECT_TYPE_INHERITS_NAME_key; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_OBJECT_TYPE"
    ADD CONSTRAINT "tbl_OBJECT_TYPE_INHERITS_NAME_key" UNIQUE ("INHERITS", "NAME");


--
-- TOC entry 1817 (class 2606 OID 55829)
-- Dependencies: 1519 1519
-- Name: tbl_OBJECT_TYPE_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_OBJECT_TYPE"
    ADD CONSTRAINT "tbl_OBJECT_TYPE_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1819 (class 2606 OID 55831)
-- Dependencies: 1520 1520
-- Name: tbl_SCHEMA_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_SCHEMA"
    ADD CONSTRAINT "tbl_SCHEMA_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1821 (class 2606 OID 55833)
-- Dependencies: 1521 1521
-- Name: tbl_TABLE_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_TABLE"
    ADD CONSTRAINT "tbl_TABLE_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1827 (class 2606 OID 55835)
-- Dependencies: 1523 1523
-- Name: tbl_VALUE_TYPE_NAME_key; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_VALUE_TYPE"
    ADD CONSTRAINT "tbl_VALUE_TYPE_NAME_key" UNIQUE ("NAME");


--
-- TOC entry 1829 (class 2606 OID 55837)
-- Dependencies: 1523 1523
-- Name: tbl_VALUE_TYPE_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_VALUE_TYPE"
    ADD CONSTRAINT "tbl_VALUE_TYPE_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1823 (class 2606 OID 55839)
-- Dependencies: 1522 1522 1522
-- Name: tbl_VALUE_VALUE_TYPE_NAME_key; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_VALUE"
    ADD CONSTRAINT "tbl_VALUE_VALUE_TYPE_NAME_key" UNIQUE ("VALUE_TYPE", "NAME");


--
-- TOC entry 1825 (class 2606 OID 55841)
-- Dependencies: 1522 1522
-- Name: tbl_VALUE_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_VALUE"
    ADD CONSTRAINT "tbl_VALUE_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1830 (class 2606 OID 55842)
-- Dependencies: 1518 1812 1518
-- Name: tbl_COLUMN_RELATES_TO_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_COLUMN"
    ADD CONSTRAINT "tbl_COLUMN_RELATES_TO_fkey" FOREIGN KEY ("RELATES_TO") REFERENCES "tbl_COLUMN"("ID") ON DELETE SET NULL;


--
-- TOC entry 1831 (class 2606 OID 55847)
-- Dependencies: 1820 1521 1518
-- Name: tbl_COLUMN_TABLE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_COLUMN"
    ADD CONSTRAINT "tbl_COLUMN_TABLE_fkey" FOREIGN KEY ("TABLE") REFERENCES "tbl_TABLE"("ID") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1832 (class 2606 OID 55852)
-- Dependencies: 1523 1828 1518
-- Name: tbl_COLUMN_VALUE_TYPE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_COLUMN"
    ADD CONSTRAINT "tbl_COLUMN_VALUE_TYPE_fkey" FOREIGN KEY ("VALUE_TYPE") REFERENCES "tbl_VALUE_TYPE"("ID") ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 1833 (class 2606 OID 55857)
-- Dependencies: 1816 1519 1519
-- Name: tbl_OBJECT_TYPE_INHERITS_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_OBJECT_TYPE"
    ADD CONSTRAINT "tbl_OBJECT_TYPE_INHERITS_fkey" FOREIGN KEY ("INHERITS") REFERENCES "tbl_OBJECT_TYPE"("ID") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 1834 (class 2606 OID 55862)
-- Dependencies: 1519 1521 1820
-- Name: tbl_OBJECT_TYPE_OBJECT_TYPE_TABLE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_OBJECT_TYPE"
    ADD CONSTRAINT "tbl_OBJECT_TYPE_OBJECT_TYPE_TABLE_fkey" FOREIGN KEY ("OBJECT_TYPE_TABLE") REFERENCES "tbl_TABLE"("ID");


--
-- TOC entry 1835 (class 2606 OID 55867)
-- Dependencies: 1521 1519 1820
-- Name: tbl_OBJECT_TYPE_TABLE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_OBJECT_TYPE"
    ADD CONSTRAINT "tbl_OBJECT_TYPE_TABLE_fkey" FOREIGN KEY ("DATA_TABLE") REFERENCES "tbl_TABLE"("ID") ON UPDATE RESTRICT ON DELETE SET NULL;


--
-- TOC entry 1836 (class 2606 OID 55872)
-- Dependencies: 1820 1521 1521
-- Name: tbl_TABLE_INHERITS_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_TABLE"
    ADD CONSTRAINT "tbl_TABLE_INHERITS_fkey" FOREIGN KEY ("INHERITS") REFERENCES "tbl_TABLE"("ID") ON DELETE CASCADE;


--
-- TOC entry 1837 (class 2606 OID 55877)
-- Dependencies: 1520 1521 1818
-- Name: tbl_TABLE_SCHEMA_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_TABLE"
    ADD CONSTRAINT "tbl_TABLE_SCHEMA_fkey" FOREIGN KEY ("SCHEMA") REFERENCES "tbl_SCHEMA"("ID") ON DELETE CASCADE;


--
-- TOC entry 1839 (class 2606 OID 55882)
-- Dependencies: 1820 1523 1521
-- Name: tbl_VALUE_TYPE_TABLE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_VALUE_TYPE"
    ADD CONSTRAINT "tbl_VALUE_TYPE_TABLE_fkey" FOREIGN KEY ("TABLE") REFERENCES "tbl_TABLE"("ID") ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 1838 (class 2606 OID 55887)
-- Dependencies: 1523 1828 1522
-- Name: tbl_VALUE_VALUE_TYPE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_VALUE"
    ADD CONSTRAINT "tbl_VALUE_VALUE_TYPE_fkey" FOREIGN KEY ("VALUE_TYPE") REFERENCES "tbl_VALUE_TYPE"("ID") ON UPDATE RESTRICT ON DELETE CASCADE;


-- Completed on 2015-10-19 22:44:07

--
-- PostgreSQL database dump complete
--

