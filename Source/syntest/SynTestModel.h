#ifndef _DB_SYNTEST_MODEL_H_
#define _DB_SYNTEST_MODEL_H_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include "orm/v1/Model.h"

#include "DbSyntestDeclaration.h"

namespace db_syntest {

class DECL_DB_SYNTEST_DIR SynTestModel
    : public orm::v1::Model< SynTestModel>
{
  public:
    SynTestModel(const orm::v1::Conn& ormConn = orm::v1::Conn());
    virtual ~SynTestModel();

  public:
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TestPoint, StTestPoint, int32_t)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TransState, StTransState, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::SysPoint, StSysPoint, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::SimPoint, StSimPoint, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::System, StSystem, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TestBatch, StTestBatch, int32_t)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::NextId, StNextId, int32_t)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TestLog, StTestLog, int32_t)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TestSystem, StTestSystem, int32_t)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TestValue, StTestValue, int32_t)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TestValueRun, StTestValueRun, int32_t)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TransValue, StTransValue, int32_t)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TestRun, StTestRun, int32_t)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TestSysValue, StTestSysValue, int32_t)
    ORM_V1_MODEL_DECLARE_STORABLE(db_syntest::st::TestSysValueRun, StTestSysValueRun, int32_t)
};

}

#endif /* _DB_SYNTEST_MODEL_H_ */
