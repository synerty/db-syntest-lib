#include "SynTestModel.h"

#include <orm/v1/ModelWrapper.ini>

// Add explicit instantiations for the model wrapper
template class orm::v1::Model< db_syntest::SynTestModel >;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TestPoint>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TransState>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::SysPoint>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::SimPoint>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::System>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TestBatch>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::NextId>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TestLog>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TestSystem>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TestValue>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TestValueRun>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TransValue>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TestRun>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TestSysValue>;
template class orm::v1::ModelWrapper<db_syntest::SynTestModel, db_syntest::st::TestSysValueRun>;

namespace db_syntest {

SynTestModel::SynTestModel(const orm::v1::Conn& ormConn)
  : BaseModel(ormConn)
{
  addStorageWrapper(mStTestPointStorageWrapper);
  addStorageWrapper(mStTransStateStorageWrapper);
  addStorageWrapper(mStSysPointStorageWrapper);
  addStorageWrapper(mStSimPointStorageWrapper);
  addStorageWrapper(mStSystemStorageWrapper);
  addStorageWrapper(mStTestBatchStorageWrapper);
  addStorageWrapper(mStNextIdStorageWrapper);
  addStorageWrapper(mStTestLogStorageWrapper);
  addStorageWrapper(mStTestSystemStorageWrapper);
  addStorageWrapper(mStTestValueStorageWrapper);
  addStorageWrapper(mStTestValueRunStorageWrapper);
  addStorageWrapper(mStTransValueStorageWrapper);
  addStorageWrapper(mStTestRunStorageWrapper);
  addStorageWrapper(mStTestSysValueStorageWrapper);
  addStorageWrapper(mStTestSysValueRunStorageWrapper);
}
// ----------------------------------------------------------------------------

SynTestModel::~SynTestModel()
{
}
// ----------------------------------------------------------------------------

    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TestPoint, StTestPoint, int32_t)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TransState, StTransState, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::SysPoint, StSysPoint, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::SimPoint, StSimPoint, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::System, StSystem, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TestBatch, StTestBatch, int32_t)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::NextId, StNextId, int32_t)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TestLog, StTestLog, int32_t)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TestSystem, StTestSystem, int32_t)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TestValue, StTestValue, int32_t)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TestValueRun, StTestValueRun, int32_t)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TransValue, StTransValue, int32_t)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TestRun, StTestRun, int32_t)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TestSysValue, StTestSysValue, int32_t)
    ORM_V1_MODEL_DEFINE_STORABLE(SynTestModel, db_syntest::st::TestSysValueRun, StTestSysValueRun, int32_t)

}
