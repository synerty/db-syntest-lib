#include "StTestValueRun.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"
#include "../st/StTestValue.h"
#include "../st/StTestRun.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestValueRun, int32_t, id) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TestValueRun, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TestValueRun>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestValueRun, date, Date, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestValueRun, id, Id, int32_t)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestValueRun, testSysValueRunTestValueRuns, TestSysValueRunTestValueRuns, db_syntest::st::TestSysValueRun)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestValueRun, testValue, TestValue, int32_t, db_syntest::st::TestValue)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestValueRun, testRun, TestRun, int32_t, db_syntest::st::TestRun)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TestValueRun)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TestValueRun::TestValueRun()
{
  mPropDate.setup(this);
  mPropId.setup(this);
  mPropTestValue.setup(this);
  mPropTestRun.setup(this);

  // Related back references.
  mPropTestSysValueRunTestValueRuns.setup(this);
}
// ----------------------------------------------------------------------------

TestValueRun::~TestValueRun()
{
}
// ----------------------------------------------------------------------------

bool
TestValueRun::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTestValueRun;
  sModelRemoveFunction = &ModelT::removeStTestValueRun;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TestValueRun > CfgT;

  CfgT::setSqlOpts()->setName("St - Test Value Run");
  CfgT::setSqlOpts()->setTableName("TEST_VALUE_RUN");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(DatePropT::setupStatic(
    "DATE",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(TestValuePropT::setupStatic(
    "TEST_VALUE",
    &db_syntest::st::TestValue::id,
    &SynTestModel::StTestValue,
    &db_syntest::st::TestValue::TestValueRunTestValues,
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(TestRunPropT::setupStatic(
    "TEST_RUN",
    &db_syntest::st::TestRun::id,
    &SynTestModel::StTestRun,
    &db_syntest::st::TestRun::TestValueRunTestRuns,
    false,
    false,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
