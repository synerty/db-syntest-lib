#include "StSysPoint.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"
#include "../st/StSimPoint.h"
#include "../st/StSystem.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::SysPoint, std::string, id) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::SysPoint, std::string>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::SysPoint>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SysPoint, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SysPoint, function, Function, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SysPoint, id, Id, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::SysPoint, testSysValueSysPoints, TestSysValueSysPoints, db_syntest::st::TestSysValue)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SysPoint, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SysPoint, requiresUser, RequiresUser, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SysPoint, stateTranslation, StateTranslation, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SysPoint, valueTranslation, ValueTranslation, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::SysPoint, simPoint, SimPoint, std::string, db_syntest::st::SimPoint)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::SysPoint, system, System, std::string, db_syntest::st::System)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SysPoint, pid, Pid, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SysPoint, sup2, Sup2, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SysPoint, sup1, Sup1, std::string)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(SysPoint)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

SysPoint::SysPoint()
{
  mPropAlias.setup(this);
  mPropFunction.setup(this);
  mPropId.setup(this);
  mPropName.setup(this);
  mPropRequiresUser.setup(this);
  mPropStateTranslation.setup(this);
  mPropValueTranslation.setup(this);
  mPropSimPoint.setup(this);
  mPropSystem.setup(this);
  mPropPid.setup(this);
  mPropSup2.setup(this);
  mPropSup1.setup(this);

  // Related back references.
  mPropTestSysValueSysPoints.setup(this);
}
// ----------------------------------------------------------------------------

SysPoint::~SysPoint()
{
}
// ----------------------------------------------------------------------------

bool
SysPoint::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStSysPoint;
  sModelRemoveFunction = &ModelT::removeStSysPoint;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::SysPoint > CfgT;

  CfgT::setSqlOpts()->setName("St - Sys Point");
  CfgT::setSqlOpts()->setTableName("SYS_POINT");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "ALIAS",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(FunctionPropT::setupStatic(
    "FUNCTION",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(NamePropT::setupStatic(
    "NAME",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(RequiresUserPropT::setupStatic(
    "REQUIRES_USER",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(StateTranslationPropT::setupStatic(
    "STATE_TRANSLATION",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ValueTranslationPropT::setupStatic(
    "VALUE_TRANSLATION",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SimPointPropT::setupStatic(
    "SIM_POINT",
    &db_syntest::st::SimPoint::simAlias,
    &SynTestModel::StSimPoint,
    &db_syntest::st::SimPoint::SysPointSimPoints,
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SystemPropT::setupStatic(
    "SYSTEM",
    &db_syntest::st::System::name,
    &SynTestModel::StSystem,
    &db_syntest::st::System::SysPointSystems,
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(PidPropT::setupStatic(
    "PID",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(Sup2PropT::setupStatic(
    "SUP2",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(Sup1PropT::setupStatic(
    "SUP1",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
