#include "StSimPoint.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::SimPoint, std::string, simAlias) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::SimPoint, std::string>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::SimPoint>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SimPoint, feedbackTimeout, FeedbackTimeout, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SimPoint, type, Type, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SimPoint, aoEngMax, AoEngMax, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SimPoint, aoEngMin, AoEngMin, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::SimPoint, simAlias, SimAlias, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::SimPoint, sysPointSimPoints, SysPointSimPoints, db_syntest::st::SysPoint)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::SimPoint, testPointSimPoints, TestPointSimPoints, db_syntest::st::TestPoint)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(SimPoint)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

SimPoint::SimPoint()
{
  mPropFeedbackTimeout.setup(this);
  mPropType.setup(this);
  mPropAoEngMax.setup(this);
  mPropAoEngMin.setup(this);
  mPropSimAlias.setup(this);

  // Related back references.
  mPropSysPointSimPoints.setup(this);
  mPropTestPointSimPoints.setup(this);
}
// ----------------------------------------------------------------------------

SimPoint::~SimPoint()
{
}
// ----------------------------------------------------------------------------

bool
SimPoint::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStSimPoint;
  sModelRemoveFunction = &ModelT::removeStSimPoint;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::SimPoint > CfgT;

  CfgT::setSqlOpts()->setName("St - Sim Point");
  CfgT::setSqlOpts()->setTableName("SIM_POINT");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(FeedbackTimeoutPropT::setupStatic(
    "FEEDBACK_TIMEOUT",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(TypePropT::setupStatic(
    "TYPE",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AoEngMaxPropT::setupStatic(
    "AO_ENG_MAX",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(AoEngMinPropT::setupStatic(
    "AO_ENG_MIN",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(SimAliasPropT::setupStatic(
    "SIM_ALIAS",
    true,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
