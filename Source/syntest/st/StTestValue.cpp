#include "StTestValue.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"
#include "../st/StTestPoint.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestValue, int32_t, id) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TestValue, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TestValue>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestValue, testPoint, TestPoint, int32_t, db_syntest::st::TestPoint)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestValue, simDigState, SimDigState, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestValue, simValue, SimValue, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestValue, id, Id, int32_t)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestValue, testSysValueTestValues, TestSysValueTestValues, db_syntest::st::TestSysValue)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestValue, testValueRunTestValues, TestValueRunTestValues, db_syntest::st::TestValueRun)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestValue, simDigValue, SimDigValue, std::string)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TestValue)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TestValue::TestValue()
{
  mPropTestPoint.setup(this);
  mPropSimDigState.setup(this);
  mPropSimValue.setup(this);
  mPropId.setup(this);
  mPropSimDigValue.setup(this);

  // Related back references.
  mPropTestSysValueTestValues.setup(this);
  mPropTestValueRunTestValues.setup(this);
}
// ----------------------------------------------------------------------------

TestValue::~TestValue()
{
}
// ----------------------------------------------------------------------------

bool
TestValue::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTestValue;
  sModelRemoveFunction = &ModelT::removeStTestValue;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TestValue > CfgT;

  CfgT::setSqlOpts()->setName("St - Test Value");
  CfgT::setSqlOpts()->setTableName("TEST_VALUE");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(TestPointPropT::setupStatic(
    "TEST_POINT",
    &db_syntest::st::TestPoint::id,
    &SynTestModel::StTestPoint,
    &db_syntest::st::TestPoint::TestValueTestPoints,
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SimDigStatePropT::setupStatic(
    "SIM_DIG_STATE",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SimValuePropT::setupStatic(
    "SIM_VALUE",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SimDigValuePropT::setupStatic(
    "SIM_DIG_VALUE",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
