#include "StTransState.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TransState, std::string, name) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TransState, std::string>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TransState>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TransState, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TransState, simState, SimState, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TransState, sysState, SysState, std::string)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TransState)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TransState::TransState()
{
  mPropName.setup(this);
  mPropSimState.setup(this);
  mPropSysState.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

TransState::~TransState()
{
}
// ----------------------------------------------------------------------------

bool
TransState::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTransState;
  sModelRemoveFunction = &ModelT::removeStTransState;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TransState > CfgT;

  CfgT::setSqlOpts()->setName("St - Trans State");
  CfgT::setSqlOpts()->setTableName("TRANS_STATE");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(NamePropT::setupStatic(
    "NAME",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SimStatePropT::setupStatic(
    "SIM_STATE",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SysStatePropT::setupStatic(
    "SYS_STATE",
    false,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
