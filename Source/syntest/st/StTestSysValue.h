#ifndef _ST_TEST_SYS_VALUE_
#define _ST_TEST_SYS_VALUE_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class TestSysValue;
  class SysPoint;
  class TestValue;
  class TestSysValueRun;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestSysValue, int32_t)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR TestSysValue
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::TestSysValue >
{
  public:
    TestSysValue();
    virtual ~TestSysValue();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_RELATED_PROPERTY(testValue, TestValue, int32_t, db_syntest::st::TestValue)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testSysValueRunTestSysValues, TestSysValueRunTestSysValues, db_syntest::st::TestSysValueRun)
    ORM_V1_DECLARE_RELATED_PROPERTY(sysPoint, SysPoint, std::string, db_syntest::st::SysPoint)
};

}
}

#endif /* _ST_TEST_SYS_VALUE_ */
