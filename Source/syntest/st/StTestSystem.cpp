#include "StTestSystem.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"
#include "../st/StTestBatch.h"
#include "../st/StSystem.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestSystem, int32_t, id) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TestSystem, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TestSystem>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSystem, id, Id, int32_t)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestSystem, batch, Batch, int32_t, db_syntest::st::TestBatch)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestSystem, system, System, std::string, db_syntest::st::System)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TestSystem)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TestSystem::TestSystem()
{
  mPropId.setup(this);
  mPropBatch.setup(this);
  mPropSystem.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

TestSystem::~TestSystem()
{
}
// ----------------------------------------------------------------------------

bool
TestSystem::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTestSystem;
  sModelRemoveFunction = &ModelT::removeStTestSystem;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TestSystem > CfgT;

  CfgT::setSqlOpts()->setName("St - Test System");
  CfgT::setSqlOpts()->setTableName("TEST_SYSTEM");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(BatchPropT::setupStatic(
    "BATCH",
    &db_syntest::st::TestBatch::id,
    &SynTestModel::StTestBatch,
    &db_syntest::st::TestBatch::TestSystemBatchs,
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SystemPropT::setupStatic(
    "SYSTEM",
    &db_syntest::st::System::name,
    &SynTestModel::StSystem,
    &db_syntest::st::System::TestSystemSystems,
    false,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
