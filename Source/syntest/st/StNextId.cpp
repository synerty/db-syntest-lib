#include "StNextId.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::NextId, int32_t, pk) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::NextId, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::NextId>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::NextId, id, Id, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::NextId, pk, Pk, int32_t)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(NextId)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

NextId::NextId()
{
  mPropId.setup(this);
  mPropPk.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

NextId::~NextId()
{
}
// ----------------------------------------------------------------------------

bool
NextId::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStNextId;
  sModelRemoveFunction = &ModelT::removeStNextId;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::NextId > CfgT;

  CfgT::setSqlOpts()->setName("St - Next Id");
  CfgT::setSqlOpts()->setTableName("NEXT_ID");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(PkPropT::setupStatic(
    "PK",
    true,
    false,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
