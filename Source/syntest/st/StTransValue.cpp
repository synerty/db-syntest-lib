#include "StTransValue.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TransValue, int32_t, name) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TransValue, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TransValue>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TransValue, simValue, SimValue, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TransValue, name, Name, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TransValue, sysValue, SysValue, double)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TransValue)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TransValue::TransValue()
{
  mPropSimValue.setup(this);
  mPropName.setup(this);
  mPropSysValue.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

TransValue::~TransValue()
{
}
// ----------------------------------------------------------------------------

bool
TransValue::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTransValue;
  sModelRemoveFunction = &ModelT::removeStTransValue;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TransValue > CfgT;

  CfgT::setSqlOpts()->setName("St - Trans Value");
  CfgT::setSqlOpts()->setTableName("TRANS_VALUE");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(SimValuePropT::setupStatic(
    "SIM_VALUE",
    false,
    false,
    boost::optional<double>()));
  CfgT::addFieldMeta(NamePropT::setupStatic(
    "NAME",
    true,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SysValuePropT::setupStatic(
    "SYS_VALUE",
    false,
    false,
    boost::optional<double>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
