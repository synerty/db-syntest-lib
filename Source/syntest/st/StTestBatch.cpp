#include "StTestBatch.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestBatch, int32_t, id) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TestBatch, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TestBatch>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestBatch, id, Id, int32_t)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestBatch, testPointBatchs, TestPointBatchs, db_syntest::st::TestPoint)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestBatch, testRunBatchs, TestRunBatchs, db_syntest::st::TestRun)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestBatch, testSystemBatchs, TestSystemBatchs, db_syntest::st::TestSystem)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestBatch, testLogBatchs, TestLogBatchs, db_syntest::st::TestLog)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestBatch, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestBatch, comment, Comment, std::string)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TestBatch)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TestBatch::TestBatch()
{
  mPropId.setup(this);
  mPropName.setup(this);
  mPropComment.setup(this);

  // Related back references.
  mPropTestPointBatchs.setup(this);
  mPropTestRunBatchs.setup(this);
  mPropTestSystemBatchs.setup(this);
  mPropTestLogBatchs.setup(this);
}
// ----------------------------------------------------------------------------

TestBatch::~TestBatch()
{
}
// ----------------------------------------------------------------------------

bool
TestBatch::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTestBatch;
  sModelRemoveFunction = &ModelT::removeStTestBatch;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TestBatch > CfgT;

  CfgT::setSqlOpts()->setName("St - Test Batch");
  CfgT::setSqlOpts()->setTableName("TEST_BATCH");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(NamePropT::setupStatic(
    "NAME",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CommentPropT::setupStatic(
    "COMMENT",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
