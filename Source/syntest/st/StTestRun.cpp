#include "StTestRun.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"
#include "../st/StTestBatch.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestRun, int32_t, id) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TestRun, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TestRun>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestRun, comment, Comment, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestRun, dateFinished, DateFinished, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestRun, dateStarted, DateStarted, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestRun, id, Id, int32_t)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestRun, testLogRuns, TestLogRuns, db_syntest::st::TestLog)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestRun, testValueRunTestRuns, TestValueRunTestRuns, db_syntest::st::TestValueRun)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestRun, runNumber, RunNumber, int32_t)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestRun, batch, Batch, int32_t, db_syntest::st::TestBatch)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TestRun)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TestRun::TestRun()
{
  mPropComment.setup(this);
  mPropDateFinished.setup(this);
  mPropDateStarted.setup(this);
  mPropId.setup(this);
  mPropRunNumber.setup(this);
  mPropBatch.setup(this);

  // Related back references.
  mPropTestLogRuns.setup(this);
  mPropTestValueRunTestRuns.setup(this);
}
// ----------------------------------------------------------------------------

TestRun::~TestRun()
{
}
// ----------------------------------------------------------------------------

bool
TestRun::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTestRun;
  sModelRemoveFunction = &ModelT::removeStTestRun;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TestRun > CfgT;

  CfgT::setSqlOpts()->setName("St - Test Run");
  CfgT::setSqlOpts()->setTableName("TEST_RUN");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(CommentPropT::setupStatic(
    "COMMENT",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DateFinishedPropT::setupStatic(
    "DATE_FINISHED",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(DateStartedPropT::setupStatic(
    "DATE_STARTED",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(RunNumberPropT::setupStatic(
    "RUN_NUMBER",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(BatchPropT::setupStatic(
    "BATCH",
    &db_syntest::st::TestBatch::id,
    &SynTestModel::StTestBatch,
    &db_syntest::st::TestBatch::TestRunBatchs,
    false,
    false,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
