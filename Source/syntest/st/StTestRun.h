#ifndef _ST_TEST_RUN_
#define _ST_TEST_RUN_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class TestRun;
  class TestBatch;
  class TestValueRun;
  class TestLog;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestRun, int32_t)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR TestRun
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::TestRun >
{
  public:
    TestRun();
    virtual ~TestRun();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(comment, Comment, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dateFinished, DateFinished, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dateStarted, DateStarted, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testLogRuns, TestLogRuns, db_syntest::st::TestLog)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testValueRunTestRuns, TestValueRunTestRuns, db_syntest::st::TestValueRun)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(runNumber, RunNumber, int32_t)
    ORM_V1_DECLARE_RELATED_PROPERTY(batch, Batch, int32_t, db_syntest::st::TestBatch)
};

}
}

#endif /* _ST_TEST_RUN_ */
