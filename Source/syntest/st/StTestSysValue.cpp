#include "StTestSysValue.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"
#include "../st/StTestValue.h"
#include "../st/StSysPoint.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestSysValue, int32_t, id) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TestSysValue, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TestSysValue>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestSysValue, testValue, TestValue, int32_t, db_syntest::st::TestValue)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValue, id, Id, int32_t)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestSysValue, testSysValueRunTestSysValues, TestSysValueRunTestSysValues, db_syntest::st::TestSysValueRun)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestSysValue, sysPoint, SysPoint, std::string, db_syntest::st::SysPoint)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TestSysValue)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TestSysValue::TestSysValue()
{
  mPropTestValue.setup(this);
  mPropId.setup(this);
  mPropSysPoint.setup(this);

  // Related back references.
  mPropTestSysValueRunTestSysValues.setup(this);
}
// ----------------------------------------------------------------------------

TestSysValue::~TestSysValue()
{
}
// ----------------------------------------------------------------------------

bool
TestSysValue::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTestSysValue;
  sModelRemoveFunction = &ModelT::removeStTestSysValue;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TestSysValue > CfgT;

  CfgT::setSqlOpts()->setName("St - Test Sys Value");
  CfgT::setSqlOpts()->setTableName("TEST_SYS_VALUE");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(TestValuePropT::setupStatic(
    "TEST_VALUE",
    &db_syntest::st::TestValue::id,
    &SynTestModel::StTestValue,
    &db_syntest::st::TestValue::TestSysValueTestValues,
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SysPointPropT::setupStatic(
    "SYS_POINT",
    &db_syntest::st::SysPoint::id,
    &SynTestModel::StSysPoint,
    &db_syntest::st::SysPoint::TestSysValueSysPoints,
    false,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
