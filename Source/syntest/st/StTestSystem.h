#ifndef _ST_TEST_SYSTEM_
#define _ST_TEST_SYSTEM_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class TestSystem;
  class System;
  class TestBatch;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestSystem, int32_t)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR TestSystem
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::TestSystem >
{
  public:
    TestSystem();
    virtual ~TestSystem();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
    ORM_V1_DECLARE_RELATED_PROPERTY(batch, Batch, int32_t, db_syntest::st::TestBatch)
    ORM_V1_DECLARE_RELATED_PROPERTY(system, System, std::string, db_syntest::st::System)
};

}
}

#endif /* _ST_TEST_SYSTEM_ */
