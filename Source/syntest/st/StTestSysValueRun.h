#ifndef _ST_TEST_SYS_VALUE_RUN_
#define _ST_TEST_SYS_VALUE_RUN_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class TestSysValueRun;
  class TestValueRun;
  class TestSysValue;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestSysValueRun, int32_t)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR TestSysValueRun
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::TestSysValueRun >
{
  public:
    TestSysValueRun();
    virtual ~TestSysValueRun();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(feedbackRecieved, FeedbackRecieved, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(feedbackTime, FeedbackTime, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysDigState, SysDigState, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysPointRecieved, SysPointRecieved, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysRawDigState, SysRawDigState, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysRawValue, SysRawValue, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysValue, SysValue, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
    ORM_V1_DECLARE_RELATED_PROPERTY(testSysValue, TestSysValue, int32_t, db_syntest::st::TestSysValue)
    ORM_V1_DECLARE_RELATED_PROPERTY(testValueRun, TestValueRun, int32_t, db_syntest::st::TestValueRun)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysEventOk, SysEventOk, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysDisplayOk, SysDisplayOk, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysComment, SysComment, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysValueOk, SysValueOk, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysStateTextOk, SysStateTextOk, int32_t)
};

}
}

#endif /* _ST_TEST_SYS_VALUE_RUN_ */
