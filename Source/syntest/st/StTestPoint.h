#ifndef _ST_TEST_POINT_
#define _ST_TEST_POINT_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class TestPoint;
  class SimPoint;
  class TestBatch;
  class TestValue;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestPoint, int32_t)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR TestPoint
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::TestPoint >
{
  public:
    TestPoint();
    virtual ~TestPoint();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testValueTestPoints, TestValueTestPoints, db_syntest::st::TestValue)
    ORM_V1_DECLARE_RELATED_PROPERTY(batch, Batch, int32_t, db_syntest::st::TestBatch)
    ORM_V1_DECLARE_RELATED_PROPERTY(simPoint, SimPoint, std::string, db_syntest::st::SimPoint)
};

}
}

#endif /* _ST_TEST_POINT_ */
