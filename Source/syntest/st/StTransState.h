#ifndef _ST_TRANS_STATE_
#define _ST_TRANS_STATE_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class TransState;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TransState, std::string)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR TransState
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::TransState >
{
  public:
    TransState();
    virtual ~TransState();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(simState, SimState, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sysState, SysState, std::string)
};

}
}

#endif /* _ST_TRANS_STATE_ */
