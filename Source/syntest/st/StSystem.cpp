#include "StSystem.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::System, std::string, name) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::System, std::string>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::System>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::System, simulatorPort, SimulatorPort, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::System, name, Name, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::System, sysPointSystems, SysPointSystems, db_syntest::st::SysPoint)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::System, testSystemSystems, TestSystemSystems, db_syntest::st::TestSystem)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::System, agentPort, AgentPort, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::System, agentIp, AgentIp, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::System, simulatorAliasPostfix, SimulatorAliasPostfix, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::System, simulatorIp, SimulatorIp, std::string)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(System)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

System::System()
{
  mPropSimulatorPort.setup(this);
  mPropName.setup(this);
  mPropAgentPort.setup(this);
  mPropAgentIp.setup(this);
  mPropSimulatorAliasPostfix.setup(this);
  mPropSimulatorIp.setup(this);

  // Related back references.
  mPropSysPointSystems.setup(this);
  mPropTestSystemSystems.setup(this);
}
// ----------------------------------------------------------------------------

System::~System()
{
}
// ----------------------------------------------------------------------------

bool
System::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStSystem;
  sModelRemoveFunction = &ModelT::removeStSystem;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::System > CfgT;

  CfgT::setSqlOpts()->setName("St - System");
  CfgT::setSqlOpts()->setTableName("SYSTEM");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(SimulatorPortPropT::setupStatic(
    "SIMULATOR_PORT",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(NamePropT::setupStatic(
    "NAME",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AgentPortPropT::setupStatic(
    "AGENT_PORT",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(AgentIpPropT::setupStatic(
    "AGENT_IP",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SimulatorAliasPostfixPropT::setupStatic(
    "SIMULATOR_ALIAS_POSTFIX",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SimulatorIpPropT::setupStatic(
    "SIMULATOR_IP",
    false,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
