#include "StTestSysValueRun.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"
#include "../st/StTestSysValue.h"
#include "../st/StTestValueRun.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestSysValueRun, int32_t, id) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TestSysValueRun, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TestSysValueRun>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, feedbackRecieved, FeedbackRecieved, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, feedbackTime, FeedbackTime, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, sysDigState, SysDigState, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, sysPointRecieved, SysPointRecieved, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, sysRawDigState, SysRawDigState, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, sysRawValue, SysRawValue, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, sysValue, SysValue, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, id, Id, int32_t)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestSysValueRun, testSysValue, TestSysValue, int32_t, db_syntest::st::TestSysValue)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestSysValueRun, testValueRun, TestValueRun, int32_t, db_syntest::st::TestValueRun)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, sysEventOk, SysEventOk, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, sysDisplayOk, SysDisplayOk, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, sysComment, SysComment, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, sysValueOk, SysValueOk, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestSysValueRun, sysStateTextOk, SysStateTextOk, int32_t)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TestSysValueRun)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TestSysValueRun::TestSysValueRun()
{
  mPropFeedbackRecieved.setup(this);
  mPropFeedbackTime.setup(this);
  mPropSysDigState.setup(this);
  mPropSysPointRecieved.setup(this);
  mPropSysRawDigState.setup(this);
  mPropSysRawValue.setup(this);
  mPropSysValue.setup(this);
  mPropId.setup(this);
  mPropTestSysValue.setup(this);
  mPropTestValueRun.setup(this);
  mPropSysEventOk.setup(this);
  mPropSysDisplayOk.setup(this);
  mPropSysComment.setup(this);
  mPropSysValueOk.setup(this);
  mPropSysStateTextOk.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

TestSysValueRun::~TestSysValueRun()
{
}
// ----------------------------------------------------------------------------

bool
TestSysValueRun::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTestSysValueRun;
  sModelRemoveFunction = &ModelT::removeStTestSysValueRun;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TestSysValueRun > CfgT;

  CfgT::setSqlOpts()->setName("St - Test Sys Value Run");
  CfgT::setSqlOpts()->setTableName("TEST_SYS_VALUE_RUN");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(FeedbackRecievedPropT::setupStatic(
    "FEEDBACK_RECIEVED",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(FeedbackTimePropT::setupStatic(
    "FEEDBACK_TIME",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SysDigStatePropT::setupStatic(
    "SYS_DIG_STATE",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SysPointRecievedPropT::setupStatic(
    "SYS_POINT_RECIEVED",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SysRawDigStatePropT::setupStatic(
    "SYS_RAW_DIG_STATE",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SysRawValuePropT::setupStatic(
    "SYS_RAW_VALUE",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(SysValuePropT::setupStatic(
    "SYS_VALUE",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(TestSysValuePropT::setupStatic(
    "TEST_SYS_VALUE",
    &db_syntest::st::TestSysValue::id,
    &SynTestModel::StTestSysValue,
    &db_syntest::st::TestSysValue::TestSysValueRunTestSysValues,
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(TestValueRunPropT::setupStatic(
    "TEST_VALUE_RUN",
    &db_syntest::st::TestValueRun::id,
    &SynTestModel::StTestValueRun,
    &db_syntest::st::TestValueRun::TestSysValueRunTestValueRuns,
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SysEventOkPropT::setupStatic(
    "SYS_EVENT_OK",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SysDisplayOkPropT::setupStatic(
    "SYS_DISPLAY_OK",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SysCommentPropT::setupStatic(
    "SYS_COMMENT",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SysValueOkPropT::setupStatic(
    "SYS_VALUE_OK",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SysStateTextOkPropT::setupStatic(
    "SYS_STATE_TEXT_OK",
    false,
    true,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
