#ifndef _ST_SYSTEM_
#define _ST_SYSTEM_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class System;
  class TestSystem;
  class SysPoint;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::System, std::string)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR System
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::System >
{
  public:
    System();
    virtual ~System();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(simulatorPort, SimulatorPort, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(sysPointSystems, SysPointSystems, db_syntest::st::SysPoint)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testSystemSystems, TestSystemSystems, db_syntest::st::TestSystem)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(agentPort, AgentPort, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(agentIp, AgentIp, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(simulatorAliasPostfix, SimulatorAliasPostfix, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(simulatorIp, SimulatorIp, std::string)
};

}
}

#endif /* _ST_SYSTEM_ */
