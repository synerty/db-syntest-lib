#include "StTestPoint.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"
#include "../st/StTestBatch.h"
#include "../st/StSimPoint.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestPoint, int32_t, id) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TestPoint, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TestPoint>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestPoint, id, Id, int32_t)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_syntest::st::TestPoint, testValueTestPoints, TestValueTestPoints, db_syntest::st::TestValue)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestPoint, batch, Batch, int32_t, db_syntest::st::TestBatch)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestPoint, simPoint, SimPoint, std::string, db_syntest::st::SimPoint)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TestPoint)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TestPoint::TestPoint()
{
  mPropId.setup(this);
  mPropBatch.setup(this);
  mPropSimPoint.setup(this);

  // Related back references.
  mPropTestValueTestPoints.setup(this);
}
// ----------------------------------------------------------------------------

TestPoint::~TestPoint()
{
}
// ----------------------------------------------------------------------------

bool
TestPoint::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTestPoint;
  sModelRemoveFunction = &ModelT::removeStTestPoint;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TestPoint > CfgT;

  CfgT::setSqlOpts()->setName("St - Test Point");
  CfgT::setSqlOpts()->setTableName("TEST_POINT");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(BatchPropT::setupStatic(
    "BATCH",
    &db_syntest::st::TestBatch::id,
    &SynTestModel::StTestBatch,
    &db_syntest::st::TestBatch::TestPointBatchs,
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(SimPointPropT::setupStatic(
    "SIM_POINT",
    &db_syntest::st::SimPoint::simAlias,
    &SynTestModel::StSimPoint,
    &db_syntest::st::SimPoint::TestPointSimPoints,
    false,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
