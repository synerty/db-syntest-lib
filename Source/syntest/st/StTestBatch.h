#ifndef _ST_TEST_BATCH_
#define _ST_TEST_BATCH_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class TestBatch;
  class TestLog;
  class TestSystem;
  class TestRun;
  class TestPoint;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestBatch, int32_t)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR TestBatch
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::TestBatch >
{
  public:
    TestBatch();
    virtual ~TestBatch();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testPointBatchs, TestPointBatchs, db_syntest::st::TestPoint)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testRunBatchs, TestRunBatchs, db_syntest::st::TestRun)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testSystemBatchs, TestSystemBatchs, db_syntest::st::TestSystem)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testLogBatchs, TestLogBatchs, db_syntest::st::TestLog)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(comment, Comment, std::string)
};

}
}

#endif /* _ST_TEST_BATCH_ */
