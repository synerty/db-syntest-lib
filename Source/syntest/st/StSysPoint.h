#ifndef _ST_SYS_POINT_
#define _ST_SYS_POINT_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class SysPoint;
  class System;
  class SimPoint;
  class TestSysValue;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::SysPoint, std::string)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR SysPoint
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::SysPoint >
{
  public:
    SysPoint();
    virtual ~SysPoint();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(function, Function, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, std::string)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testSysValueSysPoints, TestSysValueSysPoints, db_syntest::st::TestSysValue)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(requiresUser, RequiresUser, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(stateTranslation, StateTranslation, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(valueTranslation, ValueTranslation, std::string)
    ORM_V1_DECLARE_RELATED_PROPERTY(simPoint, SimPoint, std::string, db_syntest::st::SimPoint)
    ORM_V1_DECLARE_RELATED_PROPERTY(system, System, std::string, db_syntest::st::System)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(pid, Pid, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sup2, Sup2, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sup1, Sup1, std::string)
};

}
}

#endif /* _ST_SYS_POINT_ */
