#ifndef _ST_SIM_POINT_
#define _ST_SIM_POINT_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class SimPoint;
  class TestPoint;
  class SysPoint;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::SimPoint, std::string)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR SimPoint
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::SimPoint >
{
  public:
    SimPoint();
    virtual ~SimPoint();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(feedbackTimeout, FeedbackTimeout, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(type, Type, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(aoEngMax, AoEngMax, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(aoEngMin, AoEngMin, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(simAlias, SimAlias, std::string)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(sysPointSimPoints, SysPointSimPoints, db_syntest::st::SysPoint)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testPointSimPoints, TestPointSimPoints, db_syntest::st::TestPoint)
};

}
}

#endif /* _ST_SIM_POINT_ */
