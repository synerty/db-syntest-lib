#ifndef _ST_NEXT_ID_
#define _ST_NEXT_ID_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class NextId;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::NextId, int32_t)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR NextId
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::NextId >
{
  public:
    NextId();
    virtual ~NextId();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(pk, Pk, int32_t)
};

}
}

#endif /* _ST_NEXT_ID_ */
