#include "StTestLog.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SynTestModel.h"
#include "../st/StTestRun.h"
#include "../st/StTestBatch.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestLog, int32_t, id) 
template class orm::v1::StorageWrapper<db_syntest::SynTestModel, db_syntest::st::TestLog, int32_t>;
template class orm::v1::StorableWrapper<db_syntest::SynTestModel, db_syntest::st::TestLog>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestLog, run, Run, int32_t, db_syntest::st::TestRun)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestLog, id, Id, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestLog, message, Message, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_syntest::st::TestLog, severity, Severity, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(db_syntest::SynTestModel, db_syntest::st::TestLog, batch, Batch, int32_t, db_syntest::st::TestBatch)
// ----------------------------------------------------------------------------

namespace db_syntest {
namespace st {

ORM_V1_CALL_STORABLE_SETUP(TestLog)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TestLog::TestLog()
{
  mPropRun.setup(this);
  mPropId.setup(this);
  mPropMessage.setup(this);
  mPropSeverity.setup(this);
  mPropBatch.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

TestLog::~TestLog()
{
}
// ----------------------------------------------------------------------------

bool
TestLog::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedStTestLog;
  sModelRemoveFunction = &ModelT::removeStTestLog;

  using namespace orm::v1;
  typedef StorableConfig< db_syntest::st::TestLog > CfgT;

  CfgT::setSqlOpts()->setName("St - Test Log");
  CfgT::setSqlOpts()->setTableName("TEST_LOG");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(RunPropT::setupStatic(
    "RUN",
    &db_syntest::st::TestRun::id,
    &SynTestModel::StTestRun,
    &db_syntest::st::TestRun::TestLogRuns,
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(MessagePropT::setupStatic(
    "MESSAGE",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SeverityPropT::setupStatic(
    "SEVERITY",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(BatchPropT::setupStatic(
    "BATCH",
    &db_syntest::st::TestBatch::id,
    &SynTestModel::StTestBatch,
    &db_syntest::st::TestBatch::TestLogBatchs,
    false,
    true,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
