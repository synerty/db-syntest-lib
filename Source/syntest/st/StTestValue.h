#ifndef _ST_TEST_VALUE_
#define _ST_TEST_VALUE_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_syntest {
  class SynTestModel;

namespace st {
  class TestValue;
  class TestPoint;
  class TestValueRun;
  class TestSysValue;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_syntest::st::TestValue, int32_t)

namespace db_syntest {
namespace st {


class DECL_DB_SYNTEST_DIR TestValue
    : public orm::v1::Storable< db_syntest::SynTestModel, db_syntest::st::TestValue >
{
  public:
    TestValue();
    virtual ~TestValue();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_RELATED_PROPERTY(testPoint, TestPoint, int32_t, db_syntest::st::TestPoint)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(simDigState, SimDigState, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(simValue, SimValue, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testSysValueTestValues, TestSysValueTestValues, db_syntest::st::TestSysValue)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(testValueRunTestValues, TestValueRunTestValues, db_syntest::st::TestValueRun)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(simDigValue, SimDigValue, std::string)
};

}
}

#endif /* _ST_TEST_VALUE_ */
