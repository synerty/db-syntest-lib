#ifndef _DB_SYNTEST_OBJECT_DECLARATIONS_H_
#define _DB_SYNTEST_OBJECT_DECLARATIONS_H_

#include <vector>
#include <boost/shared_ptr.hpp>

namespace db_syntest {

  class SynTestModel;
  typedef boost::shared_ptr< SynTestModel > SynTestModelPtrT;
  typedef std::vector< SynTestModelPtrT > SynTestModelListT;


namespace st {
  class TestSysValueRun;
  typedef boost::shared_ptr< TestSysValueRun > TestSysValueRunPtrT;
  typedef std::vector< TestSysValueRunPtrT > TestSysValueRunListT;

  class TestSysValue;
  typedef boost::shared_ptr< TestSysValue > TestSysValuePtrT;
  typedef std::vector< TestSysValuePtrT > TestSysValueListT;

  class TestRun;
  typedef boost::shared_ptr< TestRun > TestRunPtrT;
  typedef std::vector< TestRunPtrT > TestRunListT;

  class TransValue;
  typedef boost::shared_ptr< TransValue > TransValuePtrT;
  typedef std::vector< TransValuePtrT > TransValueListT;

  class TestValueRun;
  typedef boost::shared_ptr< TestValueRun > TestValueRunPtrT;
  typedef std::vector< TestValueRunPtrT > TestValueRunListT;

  class TestValue;
  typedef boost::shared_ptr< TestValue > TestValuePtrT;
  typedef std::vector< TestValuePtrT > TestValueListT;

  class TestSystem;
  typedef boost::shared_ptr< TestSystem > TestSystemPtrT;
  typedef std::vector< TestSystemPtrT > TestSystemListT;

  class TestLog;
  typedef boost::shared_ptr< TestLog > TestLogPtrT;
  typedef std::vector< TestLogPtrT > TestLogListT;

  class NextId;
  typedef boost::shared_ptr< NextId > NextIdPtrT;
  typedef std::vector< NextIdPtrT > NextIdListT;

  class TestBatch;
  typedef boost::shared_ptr< TestBatch > TestBatchPtrT;
  typedef std::vector< TestBatchPtrT > TestBatchListT;

  class System;
  typedef boost::shared_ptr< System > SystemPtrT;
  typedef std::vector< SystemPtrT > SystemListT;

  class SimPoint;
  typedef boost::shared_ptr< SimPoint > SimPointPtrT;
  typedef std::vector< SimPointPtrT > SimPointListT;

  class SysPoint;
  typedef boost::shared_ptr< SysPoint > SysPointPtrT;
  typedef std::vector< SysPointPtrT > SysPointListT;

  class TransState;
  typedef boost::shared_ptr< TransState > TransStatePtrT;
  typedef std::vector< TransStatePtrT > TransStateListT;

  class TestPoint;
  typedef boost::shared_ptr< TestPoint > TestPointPtrT;
  typedef std::vector< TestPointPtrT > TestPointListT;

}


}

#endif /* _DB_SYNTEST_OBJECT_DECLARATIONS_H_ */
