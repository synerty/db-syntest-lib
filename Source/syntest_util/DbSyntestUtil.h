/*
 * DbSynTestUtil.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef DBSYNTESTUTIL_H_
#define DBSYNTESTUTIL_H_

#if defined(BUILD_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllexport)
#elif defined(USE_DB_SYNTEST_DLL)
#define DECL_DB_SYNTEST_DIR __declspec(dllimport)
#else
#define DECL_DB_SYNTEST_DIR
#endif

#include <stdint.h>
#include <set>
#include <string>

#include "syntest/DbSyntestDeclaration.h"

namespace db_syntest
{
  typedef std::set< std::string > StrSetT;

  /// ---------------
  /// Next ID functions

  uint32_t DECL_DB_SYNTEST_DIR
  nextId(SynTestModelPtrT model, bool save = true);

  /// ---------------
  /// Test Batch functions

  StrSetT DECL_DB_SYNTEST_DIR
  getTestBatchNames(const SynTestModelPtrT& model);

  bool DECL_DB_SYNTEST_DIR
  isTestBatchNameUsed(const SynTestModelPtrT& model, const std::string& name);

  /// ---------------
  /// Test Log functions

  enum TestLogSeverityE
  {
    errorSeverity,
    warningSeverity,
    infoSeverity
  };

  void DECL_DB_SYNTEST_DIR
  logMessage(SynTestModelPtrT& model,
      const std::string& messsage,
      const TestLogSeverityE severity = errorSeverity,
      st::TestBatchPtrT batch = st::TestBatchPtrT(),
      st::TestRunPtrT run = st::TestRunPtrT());

  /// ---------------
  /// Sim Point functions

  enum SimPointTypeE
  {
    doSimPointType,
    aoSimPointType,
    aiSimPointType,
    acSimPointType,
    di1SimPointType,
    di2SimPointType,
    unknownSimPointType
  };

  SimPointTypeE DECL_DB_SYNTEST_DIR
  toSimPointType(st::SimPointPtrT simPoint);

  std::string DECL_DB_SYNTEST_DIR
  toSimPointType(SimPointTypeE type);

} /* namespace db_syntest */
#endif /* DBSYNTESTUTIL_H_ */
