/*
 * DbSynTestUtil.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "DbSynTestUtil.h"

#include <stdexcept>

#include <boost/algorithm/string/predicate.hpp>

#include "syntest/SynTestModel.h"
#include "syntest/st/StNextId.h"
#include "syntest/st/StTestBatch.h"
#include "syntest/st/StTestLog.h"
#include "syntest/st/StSimPoint.h"

using namespace db_syntest::st;

namespace db_syntest
{

  /// ---------------
  /// Next ID functions

  uint32_t
  nextId(SynTestModelPtrT model, bool save)
  {
    const NextIdListT& ids = model->StNextIds();
    if (ids.empty())
    {
      NextIdPtrT newId(new NextId());
      newId->setPk(0);
      newId->setId(0);
      newId->addToModel(model);
    }

    if (ids.size() != 1)
      throw std::runtime_error("db_syntest::nextId(...)"
          ", Only one record is allowed in the next id table");

    uint32_t id = ids.front()->id();
    ids.front()->setId(id + 1);

    if (save)
      model->save();

    return id;
  }
  /// ---------------------------------------------------------------------------

  /// ---------------
  /// Test Batch functions

  StrSetT
  getTestBatchNames(const SynTestModelPtrT& model)
  {
    StrSetT set;

    const TestBatchListT& batches = model->StTestBatchs();
    for (TestBatchListT::const_iterator itr = batches.begin();
        itr != batches.end(); itr++)
    {
      set.insert((*itr)->name());
    }

    return set;
  }
  /// ---------------------------------------------------------------------------

  bool
  isTestBatchNameUsed(const SynTestModelPtrT& model, const std::string& name)
  {
    StrSetT set = getTestBatchNames(model);

    return (set.find(name) != set.end());
  }
  /// ---------------------------------------------------------------------------

  /// ---------------
  /// Test Log functions

  void
  logMessage(SynTestModelPtrT& model,
      const std::string& messsage,
      const TestLogSeverityE severity,
      TestBatchPtrT batch,
      st::TestRunPtrT run)
  {
    TestLogPtrT l(new TestLog());

    l->setId(nextId(model));

    switch (severity)
    {
      case errorSeverity:
        l->setSeverity("ERROR");
        break;

      case warningSeverity:
        l->setSeverity("WARNING");
        break;

      case infoSeverity:
        l->setSeverity("INFO");
        break;

      default:
        throw std::runtime_error(std::string("Unhandled severity ")
            + __PRETTY_FUNCTION__);
    }

    l->setMessage(messsage);

    if (batch.get() != NULL)
    l->setBatch(batch);

    if (run.get() != NULL)
      l->setRun(run);

    l->addToModel(model);
  }
  /// ---------------------------------------------------------------------------

  /// ---------------
  /// Sim Point functions

  SimPointTypeE
  toSimPointType(SimPointPtrT simPoint)
  {
    const std::string type = simPoint->type();

    if (boost::iequals(type, "AO"))
      return aoSimPointType;

    if (boost::iequals(type, "DO"))
      return doSimPointType;

    if (boost::iequals(type, "AI"))
      return aiSimPointType;

    if (boost::iequals(type, "AC"))
      return acSimPointType;

    if (boost::iequals(type, "DI1"))
      return di1SimPointType;

    if (boost::iequals(type, "DI2"))
      return di2SimPointType;

    throw std::runtime_error(std::string("Unhandled SimPoint.Type ")
        + __PRETTY_FUNCTION__);
  }
/// ---------------------------------------------------------------------------

  std::string
  toSimPointType(SimPointTypeE type)
  {
    switch (type)
    {
      case aoSimPointType:
        return "AO";

      case doSimPointType:
        return "DO";

      case aiSimPointType:
        return "AI";

      case acSimPointType:
        return "AC";

      case di1SimPointType:
        return "DI1";

      case di2SimPointType:
        return "DI2";

      case unknownSimPointType:
        break;
    }

    throw std::runtime_error(std::string("Unhandled SimPoint.Type ")
        + __PRETTY_FUNCTION__);
  }
/// ---------------------------------------------------------------------------

} /* namespace db_syntest */
